﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour {

    // Singleton
    public static GameManager instance;

    // game states
    public GameObject mMainScreen;
    public GameObject mGamePlayScreen;
    public GameObject mReplayMenuScreen;
    public GameObject mYouWinMenuScreen;


    public enum GameState
    {
        Home, //0
        GamePlay, //1
        Win, //2 
        Replay //3
    }
    public GameState gameState;

    // Player
    public Player player;

    // lives
    public int numLives;

    // Use this for initialization
    void Start()
    {
        SetGameState((int)GameState.Home);
    }

    void Awake()
    {
        // As long as there is not an instance already set
        if (instance == null)
        {
            // store this instance in a var
            instance = this;
            // Don't delete this object if we load a new scene
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void ActivateScreens()
    {
        switch (gameState)
        {
            case GameState.Home:
                mMainScreen.SetActive(true);
                mGamePlayScreen.SetActive(false);
                mReplayMenuScreen.SetActive(false);
                mYouWinMenuScreen.SetActive(false);
                break;

            case GameState.GamePlay:
                mMainScreen.SetActive(false);
                mGamePlayScreen.SetActive(true);
                mReplayMenuScreen.SetActive(false);
                mYouWinMenuScreen.SetActive(false);
                numLives = 3;
                break;

            case GameState.Replay:
                mMainScreen.SetActive(false);
                mGamePlayScreen.SetActive(false);
                mReplayMenuScreen.SetActive(true);
                mYouWinMenuScreen.SetActive(false);
                break;

            case GameState.Win:
                mMainScreen.SetActive(false);
                mGamePlayScreen.SetActive(false);
                mReplayMenuScreen.SetActive(false);
                mYouWinMenuScreen.SetActive(true);
                break;
        }
    }
    public void SetGameState(int state)
    {
        gameState = (GameState)state;
        ActivateScreens();
    }
}
