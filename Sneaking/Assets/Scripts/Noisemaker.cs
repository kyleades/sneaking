﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Noisemaker : MonoBehaviour {

    // var for holding volume
    public float volume = 0;
    // sound decreases each frame
    public float decayPerFrameDraw = 0.016f;

    // Update is called once per frame
    void Update()
    {
        // Every frame our noise gets less, until it hits zero
        if (volume > 0)
        {
            volume -= decayPerFrameDraw;
        }
    }
}
