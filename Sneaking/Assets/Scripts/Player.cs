﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public Text alerttext;

    // transform var
    public Transform tf;
    // noise var
    public Noisemaker noisemaker;
    // movement speed var
    public float moveSpeed;
    // rotation speed var
    public float turnSpeed;
    // volume for moving
    public float moveVolume = 5;

    // Use this for initialization
    void Start()
    {
        // grab transform into a var
        tf = GetComponent<Transform>();
        // grab noisemaker
        noisemaker = GetComponent<Noisemaker>();
        alerttext.text = "Lives: " + GameManager.instance.numLives.ToString();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            // spin the ship right
            tf.Rotate(0, 0, -turnSpeed * Time.deltaTime);

        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            // spin the ship left
            tf.Rotate(0, 0, turnSpeed * Time.deltaTime);

        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            // move the ship forward
            tf.position += (tf.up * moveSpeed * Time.deltaTime);
            if (noisemaker != null)
            {
                noisemaker.volume = Mathf.Max(noisemaker.volume, moveVolume);
            }

        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            // move the ship forward
            tf.position -= (tf.up * moveSpeed * Time.deltaTime);
            if (noisemaker != null)
            {
                noisemaker.volume = Mathf.Max(noisemaker.volume, moveVolume);
            }

        }
        // fire weapon
        /*
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bullet = Instantiate<GameObject>(GameManager.instance.bullet);
            bullet.transform.position = transform.position;
        }
        */
    }
    
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "badguy")
        {

            LifeLost();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Map")
        {
            LifeLost();
        }
    }

    void LifeLost()
    {
        GameManager.instance.numLives--;
        alerttext.text = "Lives: " + GameManager.instance.numLives.ToString();
        if (GameManager.instance.numLives == 0)
        {
            // Game over
            GameManager.instance.mReplayMenuScreen.SetActive(true);
            
        }
        else
        {
           // GameManager.instance.RemoveEnemies();
           // tf.transform.position = new Vector3(0, 0, 0);
        }
    }
    
}
